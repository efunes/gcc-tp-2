from django.test import TestCase
from django.contrib.auth.models import User

class Test_Trivial(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            username='Eduardo',
            email='edu@gmail.com',
            password='password'
        )

    def test_trivial(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_redireccion(self):
        response = self.client.get("/cualquier_url/")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/")

    def test_login(self):
        response = self.client.post("/login/", data = {
            "name" : "Eduardo",
            "password": "password"
        })
        self.assertEqual(response.status_code,302)
        self.assertEqual(response.url, "/")
        
    def test_logout(self):

        self.client.login(
            username = "Eduardo",
            password = "password"
        )
        response = self.client.get("/logout/")
        self.assertEqual(response.status_code,302)
        self.assertEqual(response.url, "/login/")
