from django.contrib import admin
from django.urls import path,re_path
from django.conf.urls import url
from gcc.views import loguear,desloguear,principal,redirigir


urlpatterns = [
    path('login/',loguear,name="login"),
    path('logout/',desloguear),
    path('',principal,name="principal"),
    re_path(r'.+',redirigir)
]
