<h3>Segundo Trabajo Práctico de GCC</h3>
Semestre: Febrero-Junio 2019
<ul>
    Desplegado en: 
<li>
    Desarrollo: https://gcc-tp-2-dev.herokuapp.com
    <ul>
    Tareas: 
        <li>
            <strong>Consistencia</strong>: evalúa que la app no tenga errores de estructura.
        </li>
        <li>
            <strong>Desarrollo</strong>: despliega la versión de la app de la rama "developer" en un servidor alterno.
        </li>
    </ul>
</li>
<li>
    Homologación: microambiente volátil de pruebas
    <ul>
        Tareas:
        <li>
            <strong>Homologación</strong>: ejecuta las pruebas en un microambiente generado por el Framework (Django).
        </li>
    </ul>
</li>
<li>
    Producción: https://gcc-tp-2.herokuapp.com
    <ul>
        Tareas:
        <li>
            <strong>Producción</strong>: despliega la versión de la app de la rama "master" en el servidor principal.
        </li>
    </ul>
</li>
</ul>
Aplicación:
<ul>
    <li>
        <ul>Páginas:
            <li>Principal: una página encargada de saludar al Objeto Visualizador.</li>
            <li>Login: una página encargada de identificar al Objeto Visualizador.</li>
        </ul>
    </li>
    <li>
        <ul>Usuarios:
            <li>admin (Desarrollo y Producción).</li>
            <li>Natalia (Producción).</li>
            <li>Alan (Producción).</li>
            <li>Guillermo (Producción).</li>
            <li>El pass por defecto es: adminadmin.</li>
        </ul>
    </li>

</ul>
Nota: ciertas configuraciones para la interacción de Heroku con Django han sido directamente realizadas en el servidor de aplicaciones para hacer provecho de la GUI de Heroku.
<div>
    <i>Ningún OVNI ha sido lastimado durante el desarrollo de este trabajo.</i>
</div>
